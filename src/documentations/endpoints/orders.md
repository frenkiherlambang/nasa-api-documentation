# ORDERS

## Attribute Orders
### id
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** `unique field yang berbentuk UUID (string) ` __[otomatis dari backend]__

### details
>**<span style="color: gold;">type</span> :** `array` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan kumpulan barang yang akan diorder beserta jumlahnya. Isi dalam array:json adalah : "product_code" & "product_qty"

### destination_address
>**<span style="color: gold;">type</span> :** `array` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan alamat dari pemesan. Isi dalam array:json adalah : "province_id" & "city_id" & "district_id" & "village_id"

### created_by
>**<span style="color: gold;">type</span> :** `array` <br>
>**<span style="color: gold;">penjelasan</span> :** Informasi pengguna yang membuat order. Isi dalam array:json adalah : "user" & "office". Kemungkinan terjadi multiuser, maka dibuat array <br>

### is_paid
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan validasi apakah pemesan sudah melakukan pembayaran atau belum <br>

### is_pre_order
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan validasi apakah pemesan mengajukan pesanan sebelum tanggal pengiriman

### is_completed
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan validasi ketika pemesan sudah menerima pesanan, maka statusnya akan berubah menjadi true

### is_packed
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan validasi ketika barang sudah dikemas dan bagian pengemasan siap mengirimkan pesanan, maka statusnya akan berubah menjadi true

### is_ppn_included
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** Pajak [otomatis dari backend]

### is_valid_for_payment
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan validasi ketika barang sudah dikonfirmasi, maka barang boleh dibayar (sebelum konfirmasi pemesan tidak bisa membayar)

### is_valid_for_packing
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan validasi ketika barang sudah dibayar, maka barang dikirim ke bagian pengemasan (tidak bisa dikemas kalo belum dibayar)

### is_request_shipping_validation
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** -

### is_prev_delivery_shipped
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** -

### has_follow_up_delivery
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** Pengiriman dapat ditindaklanjuti oleh penjual atau pengemasan

### is_pending_follow_up_delivery
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** Pengiriman tidak dapat ditindaklanjuti oleh penjual atau pengemasan

### print_invoice_enabled
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** Lakukan print file invoice, setelah dilakukan print 1x maka pemesan tidak dapat melakukan print lagi dan harus hubungi admin jika masih ingin print invoice

### canceled_by_user_id
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** order dapat dibatalkan oleh beberapa pihak seperti bagian kasir, bagian pengemasan. Yang disimpan di field ini adalah id dari pengguna yang melakukan pembatalan

### canceled_from
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** order dapat dibatalkan oleh beberapa pihak seperti bagian kasir, bagian pengemasan. Yang disimpan di field ini adalah bagian dari pengguna yang melakukan pembatalan

### destination_code
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** prefix untuk kode order yang menggunakan kode office. Misalnya: "KODE_PUSAT" + "NO_PEMBELIAN" = "AB.001 - 0010" __[otomatis dari backend]__

### origin_code
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** prefix untuk kode order yang menggunakan kode office. Misalnya: "KODE_STOCKIST" + "NO_PENJUALAN" = "XY.87Y - 0010" __[otomatis dari backend]__

### notes
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** catatan secara personal terhadap barang yang sedang diorder

### status
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** status order yang sedang berlangsung, dapat diisi dengan : "pending", "completed", "canceled"

### order_type
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan variabel untuk membedakan apakah order yang dikirim bertipe pembelian atau penjualan, kedua itu memiliki endpoint yang sama. order_type berupa enum yang dapat diisi dengan "purchase", "sale"

### order_shipment
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** Order yang dibuat bisa diambil langsung oleh pembeli juga dikirim dari gudang yagn menjual, berupa enum yang dapat diisi dengan "pickup", "delivery"

### balance_type
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** berupa enum yang dapat diisi dengan "deposit_balance", "restock_balance", "stockist_balance"

### origin_office_id
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** jika order_type => "purchase" uuid dari office yang menjadi pemesan, sedangkan jika order_type => "sale" uuid dari office yang menjadi pengirim barang

### grand_total_price
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** total keseluruhan harga dari detail product yang diorder

### grand_total_report_price
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** total laporan uang dari detail product yang diorder beserta detail dari barangnya

### grand_total_weight
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** total berat dari detail product yang diorder (penting untuk menentukan jumlah koli)

### origin_warehouse_id
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan uuid dari gudang yang menjual barang

### destination_office_id
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan uuid dari office yang menjadi penerima barang

### destination_warehouse_id
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan uuid dari gudang yang menjadi penerima barang

### deletedAt
>**<span style="color: gold;">type</span> :** `softdeletes` <br>
>**<span style="color: gold;">penjelasan</span> :** Softdeletes digunakan agar file yang sudah di input tidak akan bisa dihapus.

### createdAt
>**<span style="color: gold;">type</span> :** `softdeletes` <br>
>**<span style="color: gold;">penjelasan</span> :** Tanggal dan waktu pembuatan data.

### updatedAt
>**<span style="color: gold;">type</span> :** `softdeletes` <br>
>**<span style="color: gold;">penjelasan</span> :** Tanggal dan waktu terakhir data yang diubah.

<!-- relasi -->

### origin-office
>**<span style="color: blue">type</span> :** `many to one` <br>
>**<span style="color: blue;">penjelasan</span> :** office dapat membuat banyak order sebagai pemesan/penjual

### destination-office
>**<span style="color: blue">type</span> :** `many to one` <br>
>**<span style="color: blue;">penjelasan</span> :** office dapat membuat banyak order sebagai penerima barang

### destination-warehouse
>**<span style="color: blue">type</span> :** `many to one` <br>
>**<span style="color: blue;">penjelasan</span> :** gudang dapat membuat banyak order sebagai penerima barang

### buyer-type
>**<span style="color: blue">type</span> :** `many to one` <br>
>**<span style="color: blue;">penjelasan</span> :** dalam 1x order, pemesan/penjual hanya bisa memilih satu buyer-type

### area
>**<span style="color: blue">type</span> :** `many to one` <br>
>**<span style="color: blue;">penjelasan</span> :** -

### warehouse
>**<span style="color: blue">type</span> :** `many to one` <br>
>**<span style="color: blue;">penjelasan</span> :** gudang yang dipilih dari office si pemesan/penjual (karena setiap office bisa memiliki banyak gudang)

### order-bundles
>**<span style="color: blue">type</span> :** `many to many` <br>
>**<span style="color: blue;">penjelasan</span> :** -

### order-details
>**<span style="color: blue">type</span> :** `one to many` <br>
>**<span style="color: blue;">penjelasan</span> :** -

### payments
>**<span style="color: blue">type</span> :** `one to many` <br>
>**<span style="color: blue;">penjelasan</span> :** -

### stock-movements
>**<span style="color: blue">type</span> :** `one to many` <br>
>**<span style="color: blue;">penjelasan</span> :** merupakan koli yang dikirimkan dari gudang ke gudang office penerima

## Parameters

### page[number]
>**<span style="color: salmon;">type</span> :** `integer` <br>
>**<span style="color: salmon;">deskripsi</span> :** Parameter ini digunakan untuk menampilkan data pada halaman yang dipilih.

### page[size]
>**<span style="color: salmon;">type</span> :** `integer` <br>
>**<span style="color: salmon;">deskripsi</span> :** Parameter ini digunakan untuk menentukan jumlah data yang akan ditampilkan.

### filter[is_completed]
>**<span style="color: salmon;">type</span> :** `boolean` <br>
>**<span style="color: salmon;">deskripsi</span> :** Parameter ini digunakan untuk menampilkan data yang memiliki field is_completed yang bernilai 1.

### filter[destination_office_id]
>**<span style="color: salmon;">type</span> :** `uuid` <br>
>**<span style="color: salmon;">deskripsi</span> :** Parameter ini digunakan untuk menampilkan data yang memiliki kolom destination_office_id yang sama dengan parameter ini.

### include
>**<span style="color: salmon;">type</span> :** `string` <br>
>**<span style="color: salmon;">deskripsi</span> :** Parameter ini digunakan untuk menampilkan data beserta relasinya langsung.

### sort
>**<span style="color: salmon;">type</span> :** `string` <br>
>**<span style="color: salmon;">deskripsi</span> :** Parameter ini digunakan untuk mengurutkan data. Contoh ingin mengurutkan data secara ascending berdasarkan kolom createdAt, maka parameter ini bernilai createdAt, sedangkan untuk descending, parameter ini bernilai -createdAt.

## Get Orders (ALL)
<i>endpoint untuk mengambil semua data order</i>

```json
curl -X GET http://nasa.test/api/v1/orders

"Accept": application/vnd.api+json
"Content-Type": application/vnd.api+json
"Authorization": Bearer Token
```

## ADDITIONAL INFORMATION !!!

  <span style="color: red">
  <b><i>Orders tidak memilki create, update, delete secara default, karena ketiga proses tersebut dilakukan pada flow aplikasi.</i></b>
  </span>