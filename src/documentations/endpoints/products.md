# PRODUCTS

## Attributes

### id
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** `unique field yang berbentuk UUID (string) ` __[otomatis dari backend]__

### code
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** Code bersifat unik namun perlu dilakukan input ketika user melakukan input data baru

### name
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** nama dari produk

### slug
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** slug dari produk __[otomatis dari backend]__

### description
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** penjelasan lebih lengkap dari produk

### weight
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** berat dari produk

### qty
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** jumlah produk yang tersedia

### supplier_price
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** harga dari supplier

### point
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** point bekerja sebagai reward yang dapat mempengaruhi akun dari mitra usaha (seperti naik level dari mitra ke stockist)

### bonus
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** bonus dapat bekerja seperti uang asli/ intinya dapat mempengaruhi total dari pembelian dan bonus mitra

### is_point
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** apakah produk ini dapat diberikan point

### license_number
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** nomor lisensi dari produk

### license_institution
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** institusi yang menjual produk ini

### license_start
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** tanggal mulai berlaku lisensi dari produk

### license_end
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** tanggal berakhir lisensi dari produk

### license_status
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** status lisensi dari produk

### product_prices
>**<span style="color: gold;">type</span> :** `array` <br>
>**<span style="color: gold;">penjelasan</span> :** harga dari produk

### product_limits
>**<span style="color: gold;">type</span> :** `array` <br>
>**<span style="color: gold;">penjelasan</span> :** batasan pembelian dari produk

### deletedAt
>**<span style="color: gold;">type</span> :** `softdeletes` <br>
>**<span style="color: gold;">penjelasan</span> :** Softdeletes digunakan agar file yang sudah di input tidak akan bisa dihapus. __[otomatis dari backend]__

### createdAt
>**<span style="color: gold;">type</span> :** `datetime` <br>
>**<span style="color: gold;">penjelasan</span> :** Tanggal dan waktu pembuatan data. __[otomatis dari backend]__

### updatedAt
>**<span style="color: gold;">type</span> :** `datetime` <br>
>**<span style="color: gold;">penjelasan</span> :** Tanggal dan waktu terakhir data yang diubah. __[otomatis dari backend]__

<!-- relasi -->

### product-limits
>**<span style="color: blue">type</span> :** `one to many` <br>
>**<span style="color: blue;">penjelasan</span> :** product limit dapat memiliki banyak product

### product-prices
>**<span style="color: blue">type</span> :** `one to many` <br>
>**<span style="color: blue;">penjelasan</span> :** product limit dapat memiliki banyak product

### product-category
>**<span style="color: blue">type</span> :** `many to one` <br>
>**<span style="color: blue;">penjelasan</span> :** product dapat memilih kategori barang, jadi satu kategori barang dapat memiliki banyak product

### office
>**<span style="color: blue">type</span> :** `many to one` <br>
>**<span style="color: blue;">penjelasan</span> :** product dapat memilih office/kantor, jadi satu kantor dapat memiliki banyak product

### unit
>**<span style="color: blue">type</span> :** `many to one` <br>
>**<span style="color: blue;">penjelasan</span> :** product dapat memilih unit, jadi satu unit dapat memiliki banyak product

## Parameters

### filter[search]
>**<span style="color: salmon;">type</span> :** `string` <br>
>**<span style="color: salmon;">deskripsi</span> :** Mencari produk berdasarkan nama produk (nama sebagai default filter)

## GET Products (ALL)
<i>endpoint untuk mengambil semua data order</i>

```json
curl -X GET http://nasa.test/api/v1/products

"Accept": application/vnd.api+json
"Content-Type": application/vnd.api+json
"Authorization": Bearer Token
```

### TEST VUEJS TO MARKDOWN

<Auth-login-pusat></Auth-login-pusat>