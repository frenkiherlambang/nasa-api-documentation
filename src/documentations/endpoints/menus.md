# MENUS

## Attribute Menus
### id
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** `unique field yang berbentuk UUID (string)`

### icon
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** `icon digunakan untuk menampilkan icon pada menu`

### name
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** `nama memiliki karakteristik unique`

### order_no
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** -

### type
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** ``

### url
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** -

<!-- relasi -->

### office-categories
>**<span style="color: blue">type</span> :** `many to many` <br>
>**<span style="color: blue;">penjelasan</span> :** -

### permissions
>**<span style="color: blue;">type</span> :** `many to many` <br>
>**<span style="color: blue;">penjelasan</span> :** -

### roles
>**<span style="color: blue;">type</span> :** `many to many` <br>
>**<span style="color: blue;">penjelasan</span> :** -


## Get Menus (ALL)
<i>endpoint untuk mengambil semua data menu</i>

```json
curl -X GET http://nasa.test/api/v1/menus

"Accept": application/vnd.api+json
"Content-Type": application/vnd.api+json
"Authorization": Bearer Token
```
## Create Menu
<i>endpoint untuk membuat data menu</i>

```json
curl -X POST http://nasa.test/api/v1/menus

"Accept": application/vnd.api+json
"Content-Type": application/vnd.api+json
"Authorization": Bearer Token
```
<b>Request Body __Create Menu__</b>

```json
{
  "data": {
    "type": "menus",
    "attributes": {
      "name": "Menu Utama",
      "description": "Nasi Goreng"
    }
  }
}
```

## Update Menu
<i>endpoint untuk mengupdate data menu</i>

```json
curl -X PATCH http://nasa.test/api/v1/menus/{uuid}

"Accept": application/vnd.api+json
"Content-Type": application/vnd.api+json
"Authorization": Bearer Token
```

<b>Request Body __Update Menu__</b>

```json
{
    "data": {
        "type": "menus",
        "id": "{{menu_id}}",
        "attributes": {
            "name": "Menu Update",
            "parent_id": null,
            "order_no": {{menu_order_id}},
            "url": "/test"
        }
    }
}
```

## Delete Menu
<i>endpoint untuk menghapus data menu</i>

```json
curl -X DELETE http://nasa.test/api/v1/menus/{uuid}

"Accept": application/vnd.api+json
"Content-Type": application/vnd.api+json
"Authorization": Bearer Token
```