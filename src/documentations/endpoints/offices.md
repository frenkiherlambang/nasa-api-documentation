# OFFICES
<i>Office bisa memiliki banyak user, namun user hanya dapat memiliki satu office saja; Membuat stockist tidak bisa langsung dari user, karena proses tersebut dilakukan dengan cara promosi dari mitra usaha dengan syarat-syarat tertentu. Ketika mitra usaha di promosikan ke stockist, sistem akan melakukan duplikasi terhadap data dari mitra usaha dan membuat akun serta office baru dengan type : Stockist. Intinya jadi ada 2 akun dengan 2 akses yang berbeda.</i>

## Attribute Offices
### id
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** `unique field yang berbentuk UUID (string) ` __[otomatis dari backend]__

### deposit_balance
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** Atribut ini digunakan ketika mitra usaha dipromosikan ke stockist

### deposit_multiplier
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** Atribut ini digunakan ketika mitra usaha dipromosikan ke stockist

### deposit_topup
>**<span style="color: gold;">type</span> :** `integer` <br>
>**<span style="color: gold;">penjelasan</span> :** Atribut ini digunakan ketika mitra usaha dipromosikan ke stockist

### name
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** Nama dari kantor yang dibuat

### office_registration_id
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan nomor tempat office di registrasi. Jadi yang melakukan registrasi suatu office adalah office lainnya.

### office_referral_id
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** merupakan nomor referral tempat office di registrasi. Jadi yang melakukan registrasi suatu office adalah office lainnya. Hal ini dapat mempengaruhi setiap aktifitas dari office yang didaftarkan.

### office_bonus_pickup_id
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** -

### sex
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** informasi jenis kelamin dari pemilik office (mitra usaha)

### marital_status
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** informasi status pernikahan dari pemilik office (mitra usaha)

### code
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** kode unik untuk office __[otomatis dari backend]__

### no_account_gl
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** -

### cost_center
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** -

### office_type
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** -

### ktp_number
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** -

### birthdate
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** -

### spouse_name
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** Nama pasangan dari pemilik office (mitra usaha)

### spouse_birthdate
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** Tangal lahir pasangan dari pemilik office (mitra usaha)

### devisor_name
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** -

### devisor_birthdate
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** -

### bank_account_number
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** nomor rekening

### bank_account_name
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** nama dari rekening

### bank_name
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** nama bank dari rekening

### bank_branch_name
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** cabang bank (code/nama daerah)

### npwp_name
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** nama pemilik npwp

### npwp_number
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** nomor npwp

### npwp_address
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** alamat npwp

### npwp_address2
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** alamat npwp ke-2

### is_promoted
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** sudah diprosikan atau belum

### pasif
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** apakah akun tidak aktif? lakukan pengecekan dengan cara mengambil nilai dari field ini

### urut
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** -

### address
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** alamat office (mitra usaha)

### address2
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** alamat office ke-2 (mitra usaha)

### postal_code
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** kode pos office

### area
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** area dari office (mitra usaha)

### kta
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** nomor kta dari mitra usaha

### phone
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** nomor telepon dari mitra usaha

### email
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** email dari mitra usaha

### pulau
>**<span style="color: gold;">type</span> :** `string` <br>
>**<span style="color: gold;">penjelasan</span> :** pilih pulau berdasarkan wilayah/alamat

### is_active
>**<span style="color: gold;">type</span> :** `boolean` <br>
>**<span style="color: gold;">penjelasan</span> :** apakah akun aktif atau tidak

### address_details
>**<span style="color: gold;">type</span> :** `array` <br>
>**<span style="color: gold;">penjelasan</span> :** alamat lengkap dari pemilik kantor/ alamat lengkap dari kantor

### deletedAt
>**<span style="color: gold;">type</span> :** `softdeletes` <br>
>**<span style="color: gold;">penjelasan</span> :** data office tidak dapat dihapus

### createdAt
>**<span style="color: gold;">type</span> :** `timestamp` <br>
>**<span style="color: gold;">penjelasan</span> :** 

### updatedAt
>**<span style="color: gold;">type</span> :** `timestamp` <br>
>**<span style="color: gold;">penjelasan</span> :** 

<!-- relasi -->

### warehouses
>**<span style="color: blue;">type</span> :** `many to many` <br>
>**<span style="color: blue;">penjelasan</span> :** relasi ke gudang. Office memiliki banyak gudang dan gudang bisa memiliki banyak office.

### expeditions
>**<span style="color: blue;">type</span> :** `many to many` <br>
>**<span style="color: blue;">penjelasan</span> :** 

### office-category
>**<span style="color: blue;">type</span> :** `many to one` <br>
>**<span style="color: blue;">penjelasan</span> :** office hanya dapat memiliki satu office-category

### bank
>**<span style="color: blue;">type</span> :** `many to one` <br>
>**<span style="color: blue;">penjelasan</span> :** office hanya dapat memiliki satu bank

### uplines
>**<span style="color: blue;">type</span> :** `many to many` <br>
>**<span style="color: blue;">penjelasan</span> :** __upline__ adalah distributor awal yang merekrut orang lain sebagai __downline__ ( bawahan ) dan mengenalkan downline tersebut mengenai bisnis Multi Level Marketing.

### downlines
>**<span style="color: blue;">type</span> :** `many to many` <br>
>**<span style="color: blue;">penjelasan</span> :**  __upline__ adalah distributor awal yang merekrut orang lain sebagai __downline__ ( bawahan ) dan mengenalkan downline tersebut mengenai bisnis Multi Level Marketing.

### upline-relations
>**<span style="color: blue;">type</span> :** `one to many` <br>
>**<span style="color: blue;">penjelasan</span> :** office bisa memiliki banyak upline-relations

### downline-relations
>**<span style="color: blue;">type</span> :** `one to many` <br>
>**<span style="color: blue;">penjelasan</span> :** office bisa memiliki banyak downline-relations

### users
>**<span style="color: blue;">type</span> :** `one to many` <br>
>**<span style="color: blue;">penjelasan</span> :** 

### balance-trxes
>**<span style="color: blue;">type</span> :** `one to many` <br>
>**<span style="color: blue;">penjelasan</span> :** 

### addresses
>**<span style="color: blue;">type</span> :** `one to many` <br>
>**<span style="color: blue;">penjelasan</span> :** 

### products
>**<span style="color: blue;">type</span> :** `one to many` <br>
>**<span style="color: blue;">penjelasan</span> :** 

### office-banks
>**<span style="color: blue;">type</span> :** `one to many` <br>
>**<span style="color: blue;">penjelasan</span> :** 

## Parameters

### filter[office_type]
>**<span style="color: salmon;">type</span> :** `string` <br>
>**<span style="color: salmon;">penjelasan</span> :** filter berdasarkan office_type

## Get Offices (ALL)
<i>endpoint untuk mengambil semua data offices</i>

```json
curl -X GET http://nasa.test/api/v1/offices

"Accept": application/vnd.api+json
"Content-Type": application/vnd.api+json
"Authorization": Bearer Token
```

## Create Office
<i>endpoint untuk membuat data office</i>

