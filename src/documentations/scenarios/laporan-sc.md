## Membuat Laporan Penjualan (user SC)
harus mencantumkan metode pembayaran, jumlah pembayaran (dapat multi metode) dan tipe pembayaran (menggunakan saldo atau tidak, "balance" / "non_balance")
jika menggunakan saldo, pastikan saldo sisa pembayaran (overpayment_balance) mencukupi
POST ``{{base_url}}/api/v1/order-bundles``
payload:
```{
    "data": {
        "type": "order-bundles",
        "attributes": {
            "name": "laporan SHANAS dan PGN",
            "description": "2 penjualan",
            "payment_bundles": [
                {
                    "office_bank_id": "{{office_bank_id_1}}",
                    "payment_amount": 1000000,
                    "payment_type": "non_balance"
                }  ,
                  {
                    "payment_amount": 1000000,
                    "payment_type": "balance"
                }   
            ]
        },
        "relationships": {
            "orders": {
                "data": [
                    {
                        "type": "orders",
                        "id": "{{penjualan_sc_uuid_1}}"
                    }

                ]
            },
            "office": {
                "data": {
                    "type": "offices",
                    "id": "{{user_office_id}}"
                }
            }
        }
    }
}
```
## cara mengirim laporan ke Pusat (user SC)
``{{base_url}}/api/v1/order-bundles/[order_bundle_id]``
contoh payload:
```
{
    "data": {
        "type": "order-bundles",
        "id": "{{order_bundle_id}}",
        "attributes": {
            "is_submitted": true
        }
    }
}
```

## cara memvalidasi laporan (order-bundle) (user PUSAT)
PATCH ``{{base_url}}/api/v1/order-bundles/[order_bundle_id]``
contoh payload :
```
{
    "data": {
        "type": "order-bundles",
        "id": "{{order_bundle_id}}",
        "attributes": {
            "is_validated": true
        }
    }
}
```
**Catatan**:
1. laporan (order-bundle) tidak dapat divalidasi jika ada pembayaran (payment-bundle) yang belum terverifikasi. Cara untuk memverifikasi masing-masing pembayaran dalam laporan dapat dilakukan melalui endpoint ini:
-- PATCH ``{{base_url}}/api/v1/payment-bundles/[payment_bundle_uuid_1]``
 Contoh Payload:
```
{
    "data": {
        "type": "payment-bundles",
        "id": "{{payment_bundle_uuid_1}}",
        "attributes": {
            "is_verified": true
        }
    }
}
```
untuk mengetahui uuids pembayaran laporan dapat menggunakan include relationships atau dengan endpoint dibawah ini:
GET ``{{base_url}}/api/v1/order-bundles/{{order_bundle_id}}/payment-bundles``

2. laporan yang tervalidasi akan secara otomatis menambahkan **saldo restock** ke **kantor SC** yang mengajukan.
3. jika semua pembayaran sudah terverifikasi namun total pembayaran **kurang** dari jumlah **harga laporan (bundle_report_price)** , laporan **tidak bisa divalidasi**, kecuali jika menggunakan user pusat yang memiliki permission ***order-bundle.validate.force*** yaitu manager kasir pusat atau contoh user seeder: (yohannes@naturalnusantara.co.id)
4. jika pembayaran laporan yang kurang dari harga laporan divalidasi oleh manager kasir pusat atau yang berwenang, maka kantor SC yang mengajukan laporan tersebut secara otomatis akan mendapatkan **saldo hutang** (debt_balance) dengan menerima **saldo restock penuh** (restock_balance) sesuai dengan **harga laporan (bundle_report_price)**.
5. Laporan yang sudah tervalidasi dapat ditambahkan pembayaran (payment-bundle) lagi oleh kasir pusat melalui endpoint berikut ini:
POST ``{{base_url}}/api/v1/payment-bundles``
contoh payload:
```
{
    "data": {
        "type": "payment-bundles",
        "attributes": {
            "payment_amount": 100000
        },
        "relationships": {
            "order-bundle": {
                "data": {
                    "type": "order-bundles",
                    "id": "{{order_bundle_id}}"
                }
            },
            "office-bank": {
                "data": {
                    "type": "office-banks",
                    "id": "{{office_bank_id}}"
                }
            }
        }
    }
}
```
6. pembayaran tambahan yang diverifikasi akan membayar hutang kantor SC jika ada kekurangan/selisih antara harga laporan dengan total jumlah pembayaran yang terverifikasi
7. pembayaran tambahan yang diverifikasi dan melebihi harga laporan akan masuk ke saldo sisa pembayaran kantor SC 
8. pembayaran yang sudah diverifikasi tidak dapat diubah menjadi belum terverifikasi dan tidak dapat dihapus.

## cara ambil data total laporan
GET : ``{{base_url}}/api/v1/order-bundles/-actions/getTotalTransaction?fromDate=12-12-2012&toDate=12-12-2023&office_id=[user_office_id]]``
