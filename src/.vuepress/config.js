const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'NASA API Documentation',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  base: process.env.CI_COMMIT_BRANCH === 'master'
    ? '/' + process.env.CI_PROJECT_NAME + '/'
    : '/',

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    nav: [
      {
        text: 'Tentang',
        link: '/about/',
      },
      {
        text: 'Dokumentasi',
        link: '/documentations/'
      },
    ],
    sidebar: {
      '/guide/': [
        {
          title: 'Guide',
          collapsable: false,
          children: [
            '',
            'using-vue',
          ]
        }
      ],
      '/documentations/': [
        {
          title: 'Dokumentasi',
          collapsable: false,
          children: [
            'endpoints/orders',
            'endpoints/menus',
            'endpoints/products',
            'endpoints/offices',
            'scenarios/pembelian-dari-pusat-ke-supplier',
            'scenarios/laporan-sc'
          ]
        }
      ],
      '/endpoint': [
        {
          title: "API Endpoint",
          collapsable: false,
          children: [
            '',
          ]
        }
      ]
    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
  ]
}
