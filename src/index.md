---
home: true
heroImage: https://v1.vuepress.vuejs.org/hero.png
tagline: Dokumentasi API NASA
actionText: Mulai →
actionLink: /documentations/
# features:
# - title: Feature 1 Title
#   details: Feature 1 Description
# - title: Feature 2 Title
#   details: Feature 2 Description
# - title: Feature 3 Title
  # details: Feature 3 Description
footer: Made by Prioritas Web Teknologi ❤️
---
